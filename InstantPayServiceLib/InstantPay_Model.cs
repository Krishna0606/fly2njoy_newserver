﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstantPayServiceLib
{
    public class InstantPay_Model
    {
        public class RemitterRegRequest
        {
            public string Mobile { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string PinCode { get; set; }
            public string AgentUserId { get; set; }
            public string RemitterId { get; set; }
            public string Status { get; set; }
            public string IsKYCStatus { get; set; }
            public string VerifiedStatus { get; set; }
        }

        public class RemitterRegResponse
        {
            public string RemitterId { get; set; }
            public string AgentUserId { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string IsVerified { get; set; }
            public string KYCDoc { get; set; }
            public string KYCStatus { get; set; }
            public string Mobile { get; set; }
            public string Name { get; set; }
            public string PernTxnLimit { get; set; }
            public string PinCode { get; set; }
            public string State { get; set; }
            public string CreditLimit { get; set; }
            public string ConsumedAmount { get; set; }
            public string RemainingLimit { get; set; }
            public string IMPSMode { get; set; }
            public string NEFTMode { get; set; }
        }

        public class RemitterBenDetail
        {
            public int BenID { get; set; }
            public string Name { get; set; }
            public string Mobile { get; set; }
            public string Account { get; set; }
            public string IfscCode { get; set; }
            public string RemitterId { get; set; }
            public string AgentId { get; set; }
            public string ReqBank { get; set; }
            public string BeneficiaryId { get; set; }
            public string Status { get; set; }
            public string TimeStamp { get; set; }
            public string Ipay_uuid { get; set; }
            public string OrderId { get; set; }
            public string Environment { get; set; }

            public string Bank { get; set; }
            public string imps { get; set; }
            public string lastSuccessDate { get; set; }
            public string lastSuccessImps { get; set; }
            public string lastSuccessName { get; set; }
        }

        public class FundTransfer
        {
            public int FundTransId { get; set; }
            public string AgentId { get; set; }
            public string RemitterMobile { get; set; }
            public string BenificieryId { get; set; }
            public string Amount { get; set; }
            public string TxnMode { get; set; }
            public string ipay_id { get; set; }
            public string ref_no { get; set; }
            public string opr_id { get; set; }
            public string name { get; set; }
            public string opening_bal { get; set; }
            public string charged_amt { get; set; }
            public string locked_amt { get; set; }
            public string ccf_bank { get; set; }
            public string bank_alias { get; set; }
            public string timestamp { get; set; }
            public string ipay_uuid { get; set; }
            public string orderid { get; set; }
            public string environment { get; set; }
            public string Status { get; set; }
            public string TrackId { get; set; }
            public string RemitterId { get; set; }
            public string RefundId { get; set; }
        }

        #region [Bank Detail Model]
        public class InnerBankDetail
        {
            public string id { get; set; }
            public string bank_name { get; set; }
            public string imps_enabled { get; set; }
            public string aeps_enabled { get; set; }
            public string bank_sort_name { get; set; }
            public string branch_ifsc { get; set; }
            public string ifsc_alias { get; set; }
            public string bank_iin { get; set; }
            public string is_down { get; set; }
        }

        public class BankDetail
        {
            public string statuscode { get; set; }
            public string status { get; set; }
            public List<InnerBankDetail> data { get; set; }
            public string timestamp { get; set; }
            public string ipay_uuid { get; set; }
            public string orderid { get; set; }
            public string environment { get; set; }
        }

        #endregion

        public class InitiatePayout
        {
            public int InitiatePayoutId { get; set; }
            public string AgentId { get; set; }
            public string RemitterMobile { get; set; }
            public string sp_key { get; set; }
            public string bene_name { get; set; }
            public string credit_amount { get; set; }
            public string latitude { get; set; }
            public string longitude { get; set; }
            public string endpoint_ip { get; set; }
            public string alert_mobile { get; set; }
            public string alert_email { get; set; }
            public string otp_auth { get; set; }
            public string otp { get; set; }
            public string remarks { get; set; }
            public string external_ref { get; set; }
            public string ipay_id { get; set; }
            public string transfer_value { get; set; }
            public string type_pricing { get; set; }
            public string commercial_value { get; set; }
            public string value_tds { get; set; }
            public string ccf { get; set; }
            public string vendor_ccf { get; set; }
            public string charged_amt { get; set; }
            public string payout_credit_refid { get; set; }
            public string payout_account { get; set; }
            public string payout_ifsc { get; set; }
            public string payout_name { get; set; }
            public string timestamp { get; set; }
            public string ipay_uuid { get; set; }
            public string orderid { get; set; }
            public string environment { get; set; }
            public string TrackId { get; set; }
            public string Status { get; set; }
            public string RemitterId { get; set; }
        }
    }
}
