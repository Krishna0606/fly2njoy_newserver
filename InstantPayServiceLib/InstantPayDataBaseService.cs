﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static InstantPayServiceLib.InstantPayModel;

namespace InstantPayServiceLib
{
    public static class InstantPayDataBaseService
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(InstantPayConfig.MyAmdDBConnectionString);
        #endregion

        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        #region [Connection Open and Close]
        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        #endregion


        #region [Remitter]

        public static IPDMTRemitterDetails IPDMTGetRemitterDetails(string mobile)
        {
            IPDMTRemitterDetails result = new IPDMTRemitterDetails();

            try
            {
                sqlCommand = new SqlCommand("sp_IPDMTGetRemitterDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Mobile", mobile));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "T_IPDMTRemitterDetails");
                dataTable = dataSet.Tables["T_IPDMTRemitterDetails"];
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    result.Id = dataTable.Rows[0]["ID"].ToString();
                    result.Mobile = dataTable.Rows[0]["Mobile"].ToString();
                    result.FirstName = dataTable.Rows[0]["FirstName"].ToString();
                    result.LastName = dataTable.Rows[0]["LastName"].ToString();
                    result.PinCode = dataTable.Rows[0]["PinCode"].ToString();
                    result.Mode = dataTable.Rows[0]["Mode"].ToString();
                    result.AgentId = dataTable.Rows[0]["AgentId"].ToString();
                    result.RemitterId = dataTable.Rows[0]["RemitterId"].ToString();
                    result.Status = dataTable.Rows[0]["Status"].ToString();
                    result.IsKYCStatus = dataTable.Rows[0]["IsKYCStatus"].ToString() == "0" ? false : true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public static int IPDMTInsertRemitterDetails(string mobile, string firstName, string lastName, string pinCode, string agentid, string mode)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_IPDMTInsertRemitterDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Mobile", mobile);
                sqlCommand.Parameters.AddWithValue("@FirstName", firstName);
                sqlCommand.Parameters.AddWithValue("@LastName", lastName);
                sqlCommand.Parameters.AddWithValue("@PinCode", pinCode);
                sqlCommand.Parameters.AddWithValue("@Mode", mode);
                sqlCommand.Parameters.AddWithValue("@AgentId", agentid);
                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                string id = sqlCommand.Parameters["@Id"].Value.ToString();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return 0;
        }

        public static bool IPDMTUpdateRemitterDetails(int id, string remitterId, string status, string verifiedStatus)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_IPDMTUpdateRemitterDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ID", id);
                sqlCommand.Parameters.AddWithValue("@RemitterId", remitterId);
                sqlCommand.Parameters.AddWithValue("@Status", status);
                sqlCommand.Parameters.AddWithValue("@VerifiedStatus", verifiedStatus);

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool IPDMTDeleteRemitterDetails(int id)
        {
            try
            {
                sqlCommand = new SqlCommand("delete from T_IPDMTRemitterDetails where Id=" + id, MyAmdDBConnection);

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static string GetRemmiterMobileByRemId(string remitterId)
        {
            string mobno = string.Empty;

            try
            {
                sqlCommand = new SqlCommand("select * from T_IPDMTRemitterDetails where RemitterId='" + remitterId + "'", MyAmdDBConnection);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "T_IPDMTRemitterDetails");
                dataTable = dataSet.Tables["T_IPDMTRemitterDetails"];

                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    mobno = dataTable.Rows[0]["Mobile"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return mobno;
        }

        public static bool IPDMTRemitterRegResponseLog(string mobileno, string agentid, string strresponse)
        {
            bool result = false;

            try
            {
                if (!string.IsNullOrEmpty(mobileno) && !string.IsNullOrEmpty(strresponse))
                {
                    dynamic rootDel = JObject.Parse(strresponse);
                    string statusCode = rootDel.statusCode;
                    string statusMessage = rootDel.statusMessage;
                    if (statusCode == "0" && statusMessage.ToLower() == "success")
                    {
                        dynamic remitter = rootDel.remitterDetailsData[0].remitter[0];

                        string address = remitter.address;
                        string city = remitter.city;
                        string consumedLimit = remitter.consumedLimit;
                        string id = remitter.id;
                        string isVerified = remitter.isVerified;
                        string kycDocs = remitter.kycDocs;
                        string kycStatus = remitter.kycStatus;
                        string mobile = remitter.mobile;
                        string name = remitter.name;
                        string pernTxnLimit = remitter.pernTxnLimit;
                        string pinCode = remitter.pinCode;
                        string remainingLimit = remitter.remainingLimit;
                        string state = remitter.state;

                        dynamic remitterLimit = rootDel.remitterDetailsData[0].remitterLimit[0];
                        dynamic remLimitMode = rootDel.remitterDetailsData[0].remitterLimit[0].mode;
                        dynamic remLimit = rootDel.remitterDetailsData[0].remitterLimit[0].limit;

                        string limitimpsmode = remLimitMode[0].imps;
                        string limitneftmode = remLimitMode[0].neft;
                        string limitconsumedAmount = remLimit[0].consumedAmount;
                        string limitremainningAmount = remLimit[0].remainningAmount;

                        string inputQuery = "insert into T_IPDMTRemitterRegLog ";
                        inputQuery += "(address,city,consumedLimit,id,isVerified,kycDocs,kycStatus,mobile,name,pernTxnLimit,pinCode,remainingLimit,state,limitimpsmode,limitneftmode,limitconsumedAmount,limitremainningAmount,AgentId) ";
                        inputQuery += "values ('" + address + "','" + city + "','" + consumedLimit + "','" + id + "','" + isVerified + "','" + kycDocs + "','" + kycStatus + "','" + mobile + "','" + name + "'";
                        inputQuery += ",'" + pernTxnLimit + "','" + pinCode + "','" + remainingLimit + "','" + state + "','" + limitimpsmode + "','" + limitneftmode + "','" + limitconsumedAmount + "','" + limitremainningAmount + "','" + agentid + "')";

                        if (CommonInsertAnyTable("delete from T_IPDMTRemitterRegLog where mobile='" + mobileno + "'"))
                        {
                            if (CommonInsertAnyTable(inputQuery))
                            {
                                var benificiary = rootDel.remitterDetailsData[0].benificiary;
                                if (benificiary != null)
                                {
                                    if (CommonInsertAnyTable("delete from T_IPDMTRemitterBenificiaryLog where remittermobile='" + mobileno + "'"))
                                    {
                                        foreach (var item in benificiary)
                                        {
                                            string account = item.account;
                                            string bank = item.bank;
                                            string benid = item.id;
                                            string ifsc = item.ifsc;
                                            string imps = item.imps;
                                            string lastSuccessDate = item.lastSuccessDate;
                                            string lastSuccessImps = item.lastSuccessImps;
                                            string lastSuccessName = item.lastSuccessName;
                                            string benmobile = item.mobile;
                                            string benname = item.name;
                                            string status = item.status;

                                            string inputBenQuery = "insert into T_IPDMTRemitterBenificiaryLog ";
                                            inputBenQuery += "(account,bank,id,ifsc,imps,lastSuccessDate,lastSuccessImps,lastSuccessName,mobile,name,status,remitterid,remittermobile,AgentId) ";
                                            inputBenQuery += "values('" + account + "','" + bank + "','" + benid + "','" + ifsc + "','" + imps + "','" + lastSuccessDate + "','" + lastSuccessImps + "','" + lastSuccessName + "'";
                                            inputBenQuery += ",'" + benmobile + "','" + benname + "','" + status + "','" + id + "','" + mobile + "','" + agentid + "')";
                                            if (CommonInsertAnyTable(inputBenQuery))
                                            {
                                                result = true;
                                            }
                                            else
                                            {
                                                result = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        #endregion

        #region [Beneficiary]
        public static bool IPDMTInsertBeneficiary(string accountNumber, string ifscCode, string name, string mobile, string remitterId, string agentId, string merchant, string mode)
        {
            try
            {
                bool isBenExist = CommonGetAnyTable("select * from T_IPDMTBeneficaryDetails where AccountNumber='" + accountNumber + "' and IfscCode='" + ifscCode + "' and RemitterId='" + remitterId + "'");
                if (!isBenExist)
                {
                    sqlCommand = new SqlCommand("sp_IPDMTInsertBeneficiary", MyAmdDBConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Name", name);
                    sqlCommand.Parameters.AddWithValue("@Mobile", mobile);
                    sqlCommand.Parameters.AddWithValue("@AccountNumber", accountNumber);
                    sqlCommand.Parameters.AddWithValue("@IfscCode", ifscCode);
                    sqlCommand.Parameters.AddWithValue("@RemitterId", remitterId);
                    sqlCommand.Parameters.AddWithValue("@AgentId", agentId);
                    sqlCommand.Parameters.AddWithValue("@Merchant", merchant);
                    sqlCommand.Parameters.AddWithValue("@Mode", mode);
                    //sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    //string id = sqlCommand.Parameters["@Id"].Value.ToString();
                    MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;//Convert.ToInt32(id);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool IPDMTInsertTransMoneyDetail(string agenntid, string merchant, string mode, string mobile, string benid, string amount, string txnmode)
        {
            try
            {
                //sqlCommand = new SqlCommand("sp_IPDMTInsertBeneficiary", MyAmdDBConnection);
                //sqlCommand.CommandType = CommandType.StoredProcedure;
                //sqlCommand.Parameters.AddWithValue("@AgentId", name);
                //sqlCommand.Parameters.AddWithValue("@Mobile", mobile);
                //sqlCommand.Parameters.AddWithValue("@AccountNumber", accountNumber);
                //sqlCommand.Parameters.AddWithValue("@IfscCode", ifscCode);
                //sqlCommand.Parameters.AddWithValue("@RemitterId", remitterId);
                //sqlCommand.Parameters.AddWithValue("@AgentId", agentId);
                //sqlCommand.Parameters.AddWithValue("@Merchant", merchant);
                //sqlCommand.Parameters.AddWithValue("@Mode", mode);
                ////sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                //MyAmdOpenConnection();
                //int isSuccess = sqlCommand.ExecuteNonQuery();
                ////string id = sqlCommand.Parameters["@Id"].Value.ToString();
                //MyAmdCloseConnection();

                //if (isSuccess > 0)
                //{
                //    return true;//Convert.ToInt32(id);
                //}
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        #endregion

        #region [Transfer Details]
        public static DataTable GetMoneyTransferDetailsbyAgentId(string agentId, string remitterMobile)
        {
            try
            {
                dataTable = new DataTable();

                string reqQuesry = "SELECT f.*, b.account, b.bank, b.id, b.ifsc,b.imps, b.mobile, b.name, b.status, b.remitterid,b.lastSuccessName, b.lastSuccessImps, b.lastSuccessDate"
                    + " from T_IPDMTFundTransferDetails f left join T_IPDMTRemitterBenificiaryLog b on f.BenificieryId=b.id where f.RemitterMobile='" + remitterMobile + "' and f.AgentId='" + agentId + "' order by f.CreatedDate desc";

                sqlCommand = new SqlCommand(reqQuesry, MyAmdDBConnection);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "T_IPDMTMoneyTransferDetails");
                dataTable = dataSet.Tables["T_IPDMTMoneyTransferDetails"];

                return dataTable;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static bool InsertFundTransDetails(IPDMTFundTransfer fund)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_InsertFundTransDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AgentId", fund.AgentId);
                sqlCommand.Parameters.AddWithValue("@RemitterMobile", fund.RemitterMobile);
                sqlCommand.Parameters.AddWithValue("@BenificieryId", fund.BenificieryId);
                sqlCommand.Parameters.AddWithValue("@Amount", fund.Amount);
                sqlCommand.Parameters.AddWithValue("@TxnMode", fund.TxnMode);
                sqlCommand.Parameters.AddWithValue("@IpayReferenceId", fund.IpayReferenceId);
                sqlCommand.Parameters.AddWithValue("@IpayRefNumber", fund.IpayRefNumber);
                sqlCommand.Parameters.AddWithValue("@IpayOprId", fund.IpayOprId);
                sqlCommand.Parameters.AddWithValue("@TransactionId", fund.TransactionId);
                sqlCommand.Parameters.AddWithValue("@FundTransStatus", fund.FundTransStatus);
                sqlCommand.Parameters.AddWithValue("@Merchant", fund.Merchant);
                sqlCommand.Parameters.AddWithValue("@Mode", fund.Mode);
                sqlCommand.Parameters.AddWithValue("@TrackId", fund.TrackId);

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion

        #region [Save Log]
        public static bool IPDMTInsertAllLog(string request, string response, string posturl, string actionType, string trackId = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(request) && !string.IsNullOrEmpty(response))
                {
                    string inputStr = "insert into T_IPDMTRequestResponseLog (RequestDel,ResponseDel,PostUrl,ActionType,TrackId) values ('" + request + "','" + response + "','" + posturl + "','" + actionType + "','" + trackId + "')";
                    sqlCommand = new SqlCommand(inputStr, MyAmdDBConnection);

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion

        #region [Error Log]
        public static bool IPDMTIPDMTErrorLog(string request, string response, string posturl, string errorType, string trackId = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(request) && !string.IsNullOrEmpty(response))
                {
                    trackId = !string.IsNullOrEmpty(trackId) ? trackId : string.Empty;

                    string inputStr = "insert into T_IPDMTErrorLog (RequestDel,Error,PostUrl,TrackId,ErrorType) values ('" + request + "','" + response + "','" + posturl + "','" + trackId + "','" + errorType + "')";
                    sqlCommand = new SqlCommand(inputStr, MyAmdDBConnection);

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion

        public static bool CommonInsertAnyTable(string inputQuery)
        {
            try
            {
                if (!string.IsNullOrEmpty(inputQuery))
                {
                    sqlCommand = new SqlCommand(inputQuery, MyAmdDBConnection);

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool CommonGetAnyTable(string inputQuery)
        {
            try
            {
                if (!string.IsNullOrEmpty(inputQuery))
                {
                    sqlCommand = new SqlCommand(inputQuery, MyAmdDBConnection);

                    sqlDataAdapter = new SqlDataAdapter();
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet, "CommonGetAnyTable");
                    dataTable = dataSet.Tables["CommonGetAnyTable"];

                    if (dataTable != null)
                    {
                        if (dataTable.Rows.Count > 0)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static DataTable GetAgencyDetailById(string agencyId)
        {
            try
            {
                sqlCommand = new SqlCommand("AgencyDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", agencyId));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "agent_register");
                dataTable = dataSet.Tables["agent_register"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable IPDMTFilterTransDetails(string agentId, string fromdate, string todate, string transid, string trackid)
        {
            dataTable = new DataTable();//2020-07-26

            try
            {
                string reqQuesry = "SELECT f.*, b.account, b.bank, b.id, b.ifsc,b.imps, b.mobile, b.name, b.status, b.remitterid,b.lastSuccessName, b.lastSuccessImps, b.lastSuccessDate "
                    + " from T_IPDMTFundTransferDetails f left join T_IPDMTRemitterBenificiaryLog b on f.BenificieryId=b.id ";

                string whrcondition = " where f.AgentId='" + agentId + "'";
                if (!string.IsNullOrEmpty(fromdate))
                {
                    whrcondition += " and f.CreatedDate>='" + GetDateFormate(fromdate) + "'";
                }
                if (!string.IsNullOrEmpty(todate))
                {
                    whrcondition += " and f.CreatedDate<='" + GetDateFormate(todate) + " 23:59:59'";
                }
                if (!string.IsNullOrEmpty(transid))
                {
                    whrcondition += " and TransactionId='" + transid + "'";
                }
                if (!string.IsNullOrEmpty(trackid))
                {
                    whrcondition += " and TrackId='" + trackid + "'";
                }

                reqQuesry = reqQuesry + whrcondition + " order by f.CreatedDate desc";

                sqlCommand = new SqlCommand(reqQuesry, MyAmdDBConnection);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "TableDetail");
                dataTable = dataSet.Tables["TableDetail"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        private static string GetDateFormate(string date)
        {
            return Reverse(date.Trim().Replace('/', '-'), string.Empty);
        }

        public static string Reverse(string str, string isAdd)
        {
            string date = string.Empty;
            string month = string.Empty;
            string year = string.Empty;
            string result = string.Empty;

            if (!string.IsNullOrEmpty(str))
            {
                string[] objStr = str.Split('-');

                for (int i = 0; i < objStr.Count(); i++)
                {
                    if (i == 0)
                    {
                        date = objStr[i];
                    }
                    else if (i == 1)
                    {
                        month = objStr[i];
                    }
                    else if (i == 2)
                    {
                        year = objStr[i];
                    }
                }

                if (!string.IsNullOrEmpty(date) && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(year))
                {
                    if (!string.IsNullOrEmpty(isAdd) && isAdd == "removehifan")
                    {
                        result = year + "" + month + "" + date;
                    }
                    else
                    {
                        result = year + "-" + month + "-" + date;
                    }
                }
            }

            return result;
        }
    }
}
